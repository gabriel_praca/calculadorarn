
import React from 'react';
import { View } from 'react-native';

import Entrada from './Entrada';
import Operacao from './Operacao';
import Comando from './Comando';

const Panel = props => (
    <View>
        <Entrada 
            num1={props.num1} 
            num2={props.num2} 
            updateValue={props.updateValue} 
        />
        <Operacao
            operation={props.operation}
            updateOperation={props.updateOperation}
        />
        <Comando action={props.calc} />
    </View>
);

export { Panel };
