
import React from 'react';
import { View, TextInput, StyleSheet } from 'react-native';

export default props => (
    <View>
        <TextInput 
            style={styles.txtRes}
            placeholder='Resultado'
            editable={false}
            value={props.res}
        />
    </View>
);

const styles = StyleSheet.create({
    txtRes: {
        height: 100,
        fontSize: 30,
    }
});
