
import React from 'react';
import { View, TextInput, StyleSheet } from 'react-native';

export default props => (
    <View>
        <TextInput 
            style={styles.num}
            placeholder='Numero'
            value={props.num}
            onChangeText={value => props.updateValue(props.name, value)}
        />
    </View>
);

const styles = StyleSheet.create({
    num: {
        width: 140,
        height: 80,
        fontSize: 20,
        backgroundColor: '#eeeeee'
    }
});
