
import React from 'react';
import { Picker, StyleSheet } from 'react-native';

export default class Operacao extends React.Component {
    render() {
        return (
            <Picker 
                style={styles.operation}
                selectedValue={this.props.operation}
                onValueChange={(op) => { this.props.updateOperation(op); }}
            >
                <Picker.Item label='Soma' value='soma' />
                <Picker.Item label='Subtração' value='subtracao' />
            </Picker>
        );
    }
}

const styles = StyleSheet.create({
    operation: {
        marginVertical: 15,
    }
});
