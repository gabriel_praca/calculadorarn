
import React from 'react';
import { View } from 'react-native';

import { Top, Panel, Result } from './components/Index';

export default class Main extends React.Component {
    constructor(props) {
        super(props);

        this.state = { num1: '', num2: '', operation: 'soma', res: '' };
        
        this.calc = this.calc.bind(this);
        this.updateValue = this.updateValue.bind(this);
        this.updateOperation = this.updateOperation.bind(this);
    }
    calc() {
        let res = 0;
        switch (this.state.operation) {
            case 'soma':
                res = parseFloat(this.state.num1) + parseFloat(this.state.num2);
                break;
            case 'subtracao':
                res = parseFloat(this.state.num1) - parseFloat(this.state.num2);
                break;
            default:
                res = 0;
                break;
        }

        this.setState({ res: res.toString() });
    }
    updateValue(name, num) {
        const obj = {};
        obj[name] = num;
        this.setState(obj);
    }
    updateOperation(operation) {
        this.setState({ operation });
    }
    render() {
        return (
            <View>
                <Top />
                <Result res={this.state.res} />
                <Panel 
                    num1={this.state.num1}
                    num2={this.state.num2}
                    operation={this.state.operation}
                    calc={this.calc}
                    updateOperation={this.updateOperation}
                    updateValue={this.updateValue}                
                />
            </View>
        );
    }
}
